package com.demo.springboot.dto;

public class UpdateMovieDto extends MovieDto {
    private Integer movieId;
    private String title;
    private Integer year;
    private String image;


    public UpdateMovieDto() {
    }

    public void setMovieId(Integer movieId) {
        this.movieId = movieId;
    }

    public UpdateMovieDto(Integer movieId, String title, Integer year, String image) {
        this.movieId = movieId;
        this.title = title;
        this.year = year;
        this.image = image;
    }

    public void updateMovie(Integer id) {

    }

    public Integer getMovieId() {
        return movieId;
    }

    public String getTitle() {
        return title;
    }

    public Integer getYear() {
        return year;
    }

    public String getImage() {
        return image;
    }
}
