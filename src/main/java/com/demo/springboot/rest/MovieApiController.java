package com.demo.springboot.rest;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieListDto;
import com.demo.springboot.service.MovieManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class MovieApiController {
    @Autowired
    private MovieManager movieManager;

    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);

    public MovieApiController() {
    }

    @GetMapping("/movies")
    public ResponseEntity<MovieListDto> getMovies() {
        return new ResponseEntity<>(movieManager.getMovies(), HttpStatus.OK);
    }

    @PostMapping(value = "/movies")
    public ResponseEntity<Void> addMovie(@RequestBody CreateMovieDto newMovie) {

        return movieManager.addMovie(newMovie) ? new ResponseEntity<>(HttpStatus.OK) : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/movies/{id}")
    public ResponseEntity<Void> updateMovie(@PathVariable String id) {
        if (movieManager.deleteMovie(id)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @PutMapping("/movies/{id}")
    public ResponseEntity<Void> updateMovie(@RequestBody CreateMovieDto movieDto, @PathVariable String id) {
        if (movieManager.updateMovie(movieDto, id)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
