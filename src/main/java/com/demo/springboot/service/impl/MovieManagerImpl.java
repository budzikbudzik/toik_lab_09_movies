package com.demo.springboot.service.impl;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;

import com.demo.springboot.dto.MovieListDto;
import com.demo.springboot.service.MovieManager;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class MovieManagerImpl implements MovieManager {

    private final MovieListDto movies;
    private Integer movieCount = 0;

    public MovieManagerImpl() {
        List<MovieDto> moviesList = new ArrayList<>();
        moviesList.add(new MovieDto(
                movieCount,
                "Piraci z Krzemowej Doliny",
                1999,
                "https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg")
        );
        movies = new MovieListDto(moviesList);
        movieCount++;
    }

    public MovieListDto getMovies() {
        Collections.sort(movies.getMovies(), Comparator.comparingInt(MovieDto::getMovieId));
        Collections.reverse(movies.getMovies());
        return movies;
    }

    @Override
    public boolean addMovie(CreateMovieDto newMovie) {
        if (newMovie.getImage().isBlank() || newMovie.getTitle().isBlank() || newMovie.getYear() == null) {
            return false;
        }
        movies.getMovies().add(new MovieDto(movieCount, newMovie.getTitle(), newMovie.getYear(), newMovie.getImage()));
        movieCount++;
        return true;
    }

    public boolean deleteMovie(String id) {
        Integer movieId = Integer.parseInt(id);

        for (MovieDto movie : movies.getMovies()) {
            if (movie.getMovieId() == movieId) {
                movies.getMovies().remove(movie);
                return true;
            }
        }
        return false;
    }

    public boolean updateMovie(CreateMovieDto updatedMovie, String id) {
        Integer movieId = Integer.parseInt(id);

        for (MovieDto movie : movies.getMovies()) {
            if (movie.getMovieId() == movieId) {
                if (updatedMovie.getTitle() != null) {
                    movie.setTitle(updatedMovie.getTitle());
                }
                if (updatedMovie.getImage() != null) {
                    movie.setImage(updatedMovie.getImage());
                }
                if (updatedMovie.getYear() != null) {
                    movie.setYear(updatedMovie.getYear());
                }
                return true;
            }
        }
        return false;
    }
}
