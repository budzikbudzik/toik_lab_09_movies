package com.demo.springboot.service;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieListDto;
import org.springframework.stereotype.Service;

@Service
public interface MovieManager {
    MovieListDto getMovies();

    boolean addMovie(CreateMovieDto newMovie);

    boolean deleteMovie(String id);

    boolean updateMovie(CreateMovieDto updatedMovie, String id);
}
